<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>square</title>
    <style>
        .inner_div
        {
            width:70px;
            height:70px;
            background-color: green;
            float:left;
            margin-right:5px;
            line-height: 4;
            text-align: center;
            border-radius: 50%;
        }
    </style>
</head>
<body>
<div>
    <div>
        <div class="inner_div">1</div>
        <div class="inner_div">2</div>
        <div class="inner_div">3</div>
        <div class="inner_div">4</div>
    </div>
    <div style="clear: both;"></div>

    <div style="margin-top:5px;">
        <div class="inner_div">5</div>
        <div class="inner_div">6</div>
        <div class="inner_div">7</div>
        <div class="inner_div">8</div>
    </div>

    <div style="clear: both;"></div>
    <div style="margin-top:5px;">
        <div class="inner_div">9</div>
        <div class="inner_div">10</div>
        <div class="inner_div">11</div>
        <div class="inner_div">12</div>
    </div>

    <div style="clear: both;"></div>
    <div style="margin-top:5px;">
        <div class="inner_div">13</div>
        <div class="inner_div">14</div>
        <div class="inner_div">15</div>
        <div class="inner_div">16</div>
    </div>
</div>
</body>
</html>

